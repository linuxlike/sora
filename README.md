### plymouth主题——sora
### plymouth主题，在使用前请确保你的系统安装有运行plymouth script的相关包。
# 测试使用的环境：
1. 在VMwareWorkstation虚拟机环境下进行测试，没有进行过真机测试。
2. 测试使用的linux系统为Fedora32。

# plymouth script依赖包
- plymouth-plugin-script.x86_64(64位)
- plymouth-plugin-script.i686(32位)

# 安装
- Fedora

1. cd "项目根目录"
2. sudo cp -r ./themes/sora /usr/share/plymouth/themes
3. sudo plymouth-set-default-theme sora
4. sudo plymouth-set-default-theme -R sora

- Ubuntu

1. cd "项目根目录"
2. sudo cp -r ./themes/sora /usr/share/plymouth/themes
3. sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/sora/sora.plymouth 100
4. sudo update-alternatives --config default.plymouth
5. sudo update-initramfs -u

# 主题测试
1. 执行相应的安装步骤，Fedora不执行步骤4，ubuntu不执行步骤5
2. "ctrl" + "alt" + "F1-F9"切换到其他文本模式终端
3. sudo init 3
4. cd "项目根目录"
5. sudo ./plymouth_test.sh(默认的时间是35s，可根据自己需要在脚本中修改)

# 目前的BUG
进度条回调函数中的进度数值不准确，导致某些情况下无法完整显示开机画面

# 截图(800x600 virtual box虚拟机环境)
![截图1](http://git.oschina.net/uploads/images/2017/0206/114900_2bc3fe55_1178850.png "800x600分辨率")
![截图2](http://git.oschina.net/uploads/images/2017/0206/114952_7b8fc19f_1178850.png "800x600分辨率")

# 更多plymouth主题
你可以在[gnome-look.org](https://www.gnome-look.org)进一步的定制你的系统
