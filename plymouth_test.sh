#!/bin/sh

# first check that we are in an appropriate runlevel
rlevel=$(runlevel | cut -d " " -f 2)
if [[ "$rlevel" != "2"  && "$rlevel" != "3" ]]
then
	    echo "ERROR: You must be at runlevel 2 or 3"
		    exit 1
		fi

		echo "Testing plymouth default theme ..."
		plymouthd
		sleep 1

		# check if the plymouthd daemon is alive
		plymouth --ping
		if [[ $? -eq 1 ]]
		then
			    echo "ERROR: Plymouth daemon not running"
				    exit 1
				fi

				# show the default splash screen for 5 seconds
				plymouth  --show-splash
				sleep 5

				#plymouth --ask-for-password
				#sleep 2

				# using a command rather than an option
				#plymouth ask-question --prompt="What is your name?"
				
				#timeout,the default value is 35s
				sleep 35

				plymouth --quit

				echo "Done ..."
				exit 0
